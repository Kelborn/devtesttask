Three weapons: AR4, KA47, Uzi
Weapon mechanics: shooting (damage, rpm, placeholders for sound, accuracy), reloading (duration, mag size)

Modules: 
	(AR4) : sight (changes fov and accuracy), grip (affects accuracy), suppressor (changes shooting sound)
	(Uzi) : supressor (changes shooting sound)

Replication: movement, shooting, weapon, modules, pickup, damage, weapon switching
UI: worldspace and screenspace widgets
Dispatchers: multicast delegates in Character