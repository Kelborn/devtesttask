// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "CoreMinimal.h"
#include "Components/ActorComponent.h"
#include "Inventory.generated.h"

class AABaseWeapon;
class ADevTestTaskCharacter;

UCLASS( ClassGroup=(Custom), meta=(BlueprintSpawnableComponent) )
class DEVTESTTASK_API UInventory : public UActorComponent
{
	GENERATED_BODY()

public:
	UPROPERTY(VisibleAnywhere, BlueprintReadOnly)
		AABaseWeapon* currentWeapon;

private:
	UPROPERTY()
		TArray<AABaseWeapon*> weapons;
	UPROPERTY()
		ADevTestTaskCharacter* character;

public:	
	// Sets default values for this component's properties
	UInventory();

protected:
	// Called when the game starts
	virtual void BeginPlay() override;

public:	
	// Called every frame
	virtual void TickComponent(float DeltaTime, ELevelTick TickType, FActorComponentTickFunction* ThisTickFunction) override;

	void Add(AABaseWeapon* weapon);		
	AABaseWeapon* FindWeapon(UClass* weaponClass);

	void NextWeapon();
	void PrevWeapon();
};
