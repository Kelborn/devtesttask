// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "CoreMinimal.h"
#include "GameFramework/Actor.h"
#include "ABaseWeaponModule.generated.h"

class AABaseWeapon;
class USphereComponent;

UCLASS()
class DEVTESTTASK_API AABaseWeaponModule : public AActor
{
	GENERATED_BODY()
	
public:
	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = "Components")
		UStaticMeshComponent* staticMesh;
	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = "Components")
		UClass* applicableWeapon;
	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = "Components")
		USphereComponent* pickupCollision;
	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = "Gameplay")
		FName attachSocketTag;

private:
	bool bIsInstalled;

public:	
	// Sets default values for this actor's properties
	AABaseWeaponModule();

protected:
	// Called when the game starts or when spawned
	virtual void BeginPlay() override;

public:	
	// Called every frame
	virtual void Tick(float DeltaTime) override;

	UFUNCTION()
		void OnPickupOverlapBegin(UPrimitiveComponent* OverlappedComp, AActor* OtherActor, UPrimitiveComponent* OtherComp, int32 OtherBodyIndex, bool bFromSweep, const FHitResult& SweepResult);
	
	//This method tries to get weapon that can be combined with that module
	AABaseWeapon* GetApplicableWeapon(class ADevTestTaskCharacter* OtherActor);
	//This method actually applies module
	//Should be overriden in children
	virtual void OnPickup(AABaseWeapon* weapon);
};
