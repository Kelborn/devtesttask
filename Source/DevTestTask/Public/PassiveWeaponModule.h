// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "CoreMinimal.h"
#include "ABaseWeaponModule.h"
#include "PassiveWeaponModule.generated.h"

class AABaseWeapon;

/**
 * 
 */
UCLASS()
class DEVTESTTASK_API APassiveWeaponModule : public AABaseWeaponModule
{
	GENERATED_BODY()
	
public:
	virtual void PutOnWeapon(AABaseWeapon* weapon);
	virtual void RemoveFromWeapon(AABaseWeapon* weapon);

	void OnPickup(AABaseWeapon* weapon) override;
};
