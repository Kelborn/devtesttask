// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "CoreMinimal.h"
#include "ActiveWeaponModule.h"
#include "ScopeModule.generated.h"

/**
 * 
 */
UCLASS()
class DEVTESTTASK_API AScopeModule : public AActiveWeaponModule
{
	GENERATED_BODY()
	
public:
	UPROPERTY(BlueprintReadWrite, EditAnywhere, Category = "Params")
		float targetFOV;
	UPROPERTY(BlueprintReadWrite, EditAnywhere, Category = "Params")
		float accuracyMul;
private:
	float oldFOV;
	APlayerCameraManager* cameraManager;
	bool bIsInUse;

public:
	void OnPickup(AABaseWeapon* weapon) override;

protected:
	void Use(AABaseWeapon* weapon) override;

private:
	UFUNCTION()
	void OnWeaponChanged(AABaseWeapon* weapon);

	UFUNCTION(Reliable, Client)
		void Client_BindWeaponChanged(AABaseWeapon* weapon);
		void Client_BindWeaponChanged_Implementation(AABaseWeapon* weapon);
};
