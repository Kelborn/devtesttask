// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "CoreMinimal.h"
#include "PassiveWeaponModule.h"
#include "GripModule.generated.h"

/**
 * 
 */
UCLASS()
class DEVTESTTASK_API AGripModule : public APassiveWeaponModule
{
	GENERATED_BODY()
	
protected:
	UPROPERTY(EditAnywhere, BlueprintReadWrite)
		float accuracyMul;

	void PutOnWeapon(AABaseWeapon* weapon) override;
	void RemoveFromWeapon(AABaseWeapon* weapon)override;

};
