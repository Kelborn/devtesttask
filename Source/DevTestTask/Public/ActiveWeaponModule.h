// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "CoreMinimal.h"
#include "ABaseWeaponModule.h"
#include "ActiveWeaponModule.generated.h"

class AABaseWeapon;

/**
 * 
 */
UCLASS()
class DEVTESTTASK_API AActiveWeaponModule : public AABaseWeaponModule
{
	GENERATED_BODY()
	
public:
	void OnPickup(AABaseWeapon* weapon) override;
	void TryUse(AABaseWeapon* weapon);

protected:
	virtual void Use(AABaseWeapon* weapon);
	virtual bool CanUse();	
};
