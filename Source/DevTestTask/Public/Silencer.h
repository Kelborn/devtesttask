// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "CoreMinimal.h"
#include "PassiveWeaponModule.h"
#include "Silencer.generated.h"

class USoundBase;
class AABaseWeapon;
/**
 * 
 */
UCLASS()
class DEVTESTTASK_API ASilencer : public APassiveWeaponModule
{
	GENERATED_BODY()
	
private:
	UPROPERTY()
		USoundBase* oldShootSound;

protected:
	UPROPERTY(EditAnywhere, BlueprintReadWrite)
		USoundBase* newShootSound;

	void PutOnWeapon(AABaseWeapon* weapon) override;
	void RemoveFromWeapon(AABaseWeapon* weapon)override;
};
