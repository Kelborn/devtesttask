// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "CoreMinimal.h"
#include "GameFramework/Actor.h"
#include "ABaseWeapon.generated.h"

class USphereComponent;
class AABaseWeaponModule;
class AActiveWeaponModule;
class USoundBase;
class ADevTestTaskCharacter;
class UDamageType;

UCLASS()
class DEVTESTTASK_API AABaseWeapon : public AActor
{
	GENERATED_BODY()
	
public:
	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = "Components")
		USkeletalMeshComponent* skeletalMesh;
	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = "Components")
		USceneComponent* muzzlePoint;
	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = "Components")
		USphereComponent* pickupCollision;

	UPROPERTY(EditAnywhere, BlueprintReadWrite, Replicated, Category = "Modules")
		TArray<TSubclassOf<AABaseWeaponModule>> applicableModules;
	UPROPERTY(VisibleAnywhere, BlueprintReadWrite, Replicated, Category = "Modules")
		AActiveWeaponModule* activeModule;
	UPROPERTY(VisibleAnywhere, BlueprintReadWrite, Replicated, Category = "Modules")
		TArray<AABaseWeaponModule*> installedModules;

	UPROPERTY(EditAnywhere, BlueprintReadWrite, Replicated, Category = "Stats")
		float damage;
	UPROPERTY(EditAnywhere, BlueprintReadWrite, Replicated, Category = "Stats")
		float rpm;
	UPROPERTY(EditAnywhere, BlueprintReadWrite, Replicated, Category = "Stats")
		int ammoCountTotal;
	UPROPERTY(EditAnywhere, BlueprintReadWrite, ReplicatedUsing = Rep_Ammo, Category = "Stats")
		int ammoCountInMag;
	UPROPERTY(EditAnywhere, BlueprintReadWrite, Replicated, Category = "Stats")
		int ammoCountInMagMax;
	UPROPERTY(EditAnywhere, BlueprintReadWrite, Replicated, Category = "Stats")
		float reloadDuration;
	UPROPERTY(EditAnywhere, BlueprintReadWrite, Replicated, Category = "Stats")
		float accuracy;
	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = "Stats")
		TSubclassOf<UDamageType> damageType;

	UPROPERTY(EditAnywhere, BlueprintReadWrite)
		USoundBase* shootSound;

		ADevTestTaskCharacter* playerOwner;
	UPROPERTY(Replicated)
		bool bIsHidden;


private:
	FTimerHandle reloadTimerHandle;
	float lastShotSeconds;
	bool bIsReloading;
	bool bIsPickedUp;

public:	
	// Sets default values for this actor's properties
	AABaseWeapon();
	void GetLifetimeReplicatedProps(TArray< FLifetimeProperty > & OutLifetimeProps) const override;

protected:
	// Called when the game starts or when spawned
	virtual void BeginPlay() override;

public:	
	// Called every frame
	virtual void Tick(float DeltaTime) override;

	//all active stuff
	UFUNCTION(BlueprintCallable, Category = "Shoot")
		void TryShoot();

	UFUNCTION(Reliable, Server, WithValidation)
		void Server_UseActiveModule();
		void Server_UseActiveModule_Implementation();
		bool Server_UseActiveModule_Validate() { return true; }
	UFUNCTION(Reliable, Server, WithValidation)
		void Server_Reload();
		void Server_Reload_Implementation();
		bool Server_Reload_Validate() { return true; }

		//visual hiding/showing
		void SetHidden(bool bIsHidden);
		void SetActorHidden(AActor* actor, bool bIsHidden);
	UFUNCTION(Reliable, Server, WithValidation)
		void Server_Hide();
		void Server_Hide_Implementation();
		bool Server_Hide_Validate() { return true; }
	UFUNCTION(Reliable, NetMulticast)
		void Mul_Hide();
		void Mul_Hide_Implementation();

	UFUNCTION(Reliable, Server, WithValidation)
		void Server_Show();
		void Server_Show_Implementation();
		bool Server_Show_Validate() { return true; }
	UFUNCTION(Reliable, NetMulticast)
		void Mul_Show();
		void Mul_Show_Implementation();

	//overlap handling
	UFUNCTION()
		void OnPickupOverlapBegin(UPrimitiveComponent* OverlappedComp, AActor* OtherActor, UPrimitiveComponent* OtherComp, int32 OtherBodyIndex, bool bFromSweep, const FHitResult& SweepResult);

private:
	//shooting replication
	UFUNCTION(Reliable, Server, WithValidation)
		void Server_Shoot();
		void Server_Shoot_Implementation();
		bool Server_Shoot_Validate();	
	UFUNCTION(Unreliable, NetMulticast)
		void Mul_Shoot();
		void Mul_Shoot_Implementation();
	UFUNCTION(Unreliable, NetMulticast)
		void Debug_DrawShoot(FVector startPoint, FVector endPoint);
		void Debug_DrawShoot_Implementation(FVector startPoint, FVector endPoint);

		UFUNCTION(Reliable, NetMulticast)
		void Mul_UseActiveModule();
		void Mul_UseActiveModule_Implementation();


	UFUNCTION(Reliable, NetMulticast)
		void Mul_OnPickup(AActor* OtherActor);
		void Mul_OnPickup_Implementation(AActor* OtherActor);
		FVector CalculateEndPointVector(FVector startPoint);

		bool CanShoot();

		void ReloadFinished();
	UFUNCTION()
		void Rep_Ammo();
};