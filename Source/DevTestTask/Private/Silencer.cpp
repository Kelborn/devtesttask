// Fill out your copyright notice in the Description page of Project Settings.

#include "Silencer.h"

#include "Sound/SoundBase.h"
#include "Weapons/ABaseWeapon.h"

void ASilencer::PutOnWeapon(AABaseWeapon* weapon)
{
	oldShootSound = weapon->shootSound;
	weapon->shootSound = newShootSound;
}

void ASilencer::RemoveFromWeapon(AABaseWeapon* weapon)
{
	weapon->shootSound = oldShootSound;
}