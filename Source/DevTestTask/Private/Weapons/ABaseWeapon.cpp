// Fill out your copyright notice in the Description page of Project Settings.

#include "ABaseWeapon.h"
#include "ABaseWeaponModule.h"

#include "ActiveWeaponModule.h"
#include "Kismet/GameplayStatics.h"
#include "DevTestTaskCharacter.h"
#include "Components/SphereComponent.h"
#include "Sound/SoundBase.h"
#include "UnrealNetwork.h"
#include "GameFramework/DamageType.h"

#include "DrawDebugHelpers.h"
#include "Engine.h"

// Sets default values
AABaseWeapon::AABaseWeapon()
{
 	// Set this actor to call Tick() every frame.  You can turn this off to improve performance if you don't need it.
	PrimaryActorTick.bCanEverTick = true;

	//setup weapon mesh
	skeletalMesh = CreateDefaultSubobject<USkeletalMeshComponent>(FName("WeaponMesh"));
	skeletalMesh->SetupAttachment(RootComponent);

	muzzlePoint = CreateDefaultSubobject<USceneComponent>(FName("MuzzlePoint"));
	muzzlePoint->SetupAttachment(skeletalMesh);

	//setup pickup collision
	pickupCollision = CreateDefaultSubobject<USphereComponent>(FName("PickupCollision"));
	pickupCollision->SetupAttachment(skeletalMesh);
	pickupCollision->SetSphereRadius(200.f);
	pickupCollision->SetCollisionResponseToAllChannels(ECollisionResponse::ECR_Ignore);
	pickupCollision->SetCollisionResponseToChannel(ECC_Pawn, ECollisionResponse::ECR_Overlap);
	pickupCollision->OnComponentBeginOverlap.AddDynamic(this, &AABaseWeapon::OnPickupOverlapBegin);

	//setup replication
	bReplicates = true;
	bReplicateMovement = true;
}

// Called when the game starts or when spawned
void AABaseWeapon::BeginPlay()
{
	Super::BeginPlay();
	
}

// Called every frame
void AABaseWeapon::Tick(float DeltaTime)
{
	Super::Tick(DeltaTime);

}

void AABaseWeapon::TryShoot()
{
	if (CanShoot())//client check
	{
		Server_Shoot();
	}
	else
	{
		Server_Reload();
	}
}

bool AABaseWeapon::CanShoot()
{
	if (ammoCountInMag > 0)
	{
		float secondsTillLastShot = UGameplayStatics::GetTimeSeconds(GetWorld()) - lastShotSeconds;
		return secondsTillLastShot >= 60 / rpm;
	}
	else
	{
		return false;
	}
}

FVector AABaseWeapon::CalculateEndPointVector(FVector startPoint)
{
	float maxSphereDeviation = 10 / accuracy; //or any other function to calc misaccuracy
	
	//getting second point for vector that will be used to create linetrace
	FVector randomPointInSphere = FMath::VRand() * FMath::RandRange(0.f, maxSphereDeviation) + GetActorForwardVector() + startPoint;
	return (randomPointInSphere - startPoint) * 10000 + startPoint;
}

void AABaseWeapon::Server_Shoot_Implementation()
{
	if (Role != ROLE_Authority)
	{
		return;
	}

	if (!CanShoot())//server check
	{
		return;
	}
	GEngine->AddOnScreenDebugMessage(-1, 5.f, FColor::Red, TEXT("pew-pew on server"));

	lastShotSeconds = UGameplayStatics::GetTimeSeconds(GetWorld());
	ammoCountInMag--;

	//trace check
	FCollisionQueryParams traceParams = FCollisionQueryParams(FName(TEXT("RV_Trace")), true, this);
	traceParams.bTraceComplex = true;
	traceParams.bTraceAsyncScene = true;
	traceParams.bReturnPhysicalMaterial = false;

	//Re-initialize hit info
	FHitResult hit(ForceInit);

	FVector start = muzzlePoint->GetComponentLocation();
	FVector end = CalculateEndPointVector(start);

	bool isHit = GetWorld()->LineTraceSingleByChannel(hit, start, end, ECC_Visibility, traceParams);

	if (isHit)
	{
		GEngine->AddOnScreenDebugMessage(-1, 5.f, FColor::Red, TEXT("hit something"));
		Debug_DrawShoot(start, hit.Location);
		UGameplayStatics::ApplyDamage(hit.Actor.Get(), damage, playerOwner->GetController(), this, damageType);
	}
	else
	{
		GEngine->AddOnScreenDebugMessage(-1, 5.f, FColor::Red, TEXT("hit nothing"));
		Debug_DrawShoot(start, end);
	}

	Mul_Shoot();
	Rep_Ammo();
}

bool AABaseWeapon::Server_Shoot_Validate()
{
	return true;
}

void AABaseWeapon::Mul_Shoot_Implementation()
{
	if (shootSound)
	{
		UGameplayStatics::PlaySoundAtLocation(GetWorld(), shootSound, muzzlePoint->GetComponentLocation());
	}
}

void AABaseWeapon::Debug_DrawShoot_Implementation(FVector startPoint, FVector endPoint)
{
	DrawDebugLine(GetWorld(), startPoint, endPoint, FColor(255, 0, 0), false, 2, 0, 2);
}

void AABaseWeapon::Server_Reload_Implementation()
{
	if (Role != ROLE_Authority)
	{
		return;
	}

	if (!bIsReloading && ammoCountTotal > 0)
	{
		bIsReloading = true;
		GetWorld()->GetTimerManager().SetTimer(reloadTimerHandle, this, &AABaseWeapon::ReloadFinished, reloadDuration, false);
	}
}

void AABaseWeapon::ReloadFinished()
{
	bIsReloading = false;

	if (ammoCountTotal - ammoCountInMagMax < 0)
	{
		ammoCountInMag = ammoCountTotal;
		ammoCountTotal = 0;
	}
	else
	{
		ammoCountInMag = ammoCountInMagMax;
		ammoCountTotal -= ammoCountInMagMax;
	}
}

void AABaseWeapon::Rep_Ammo()
{
	playerOwner->OnAmmoChanged();
}

void AABaseWeapon::Mul_UseActiveModule_Implementation()
{
	if (activeModule != nullptr)
	{
		activeModule->TryUse(this);
	}
}

void AABaseWeapon::Server_UseActiveModule_Implementation()
{
	if (Role != ROLE_Authority)
	{
		return;
	}
	Mul_UseActiveModule();
}

void AABaseWeapon::SetHidden(bool bIsHidden)
{
	this->bIsHidden = bIsHidden;

	SetActorHidden(this, bIsHidden);
	for (AABaseWeaponModule* mod : installedModules)
	{
		SetActorHidden(mod, bIsHidden);
	}
}

void AABaseWeapon::SetActorHidden(AActor* actor, bool bIsHidden)
{
	actor->SetActorHiddenInGame(bIsHidden);
	actor->SetActorEnableCollision(!bIsHidden);
}

void AABaseWeapon::Server_Hide_Implementation()
{
	if (Role != ROLE_Authority)
	{
		return;
	}

	Mul_Hide();
}

void AABaseWeapon::Mul_Hide_Implementation()
{

	SetHidden(true);
}

void AABaseWeapon::Server_Show_Implementation()
{
	if (Role != ROLE_Authority)
	{
		return;
	}

	Mul_Show();
}

void AABaseWeapon::Mul_Show_Implementation()
{
	SetHidden(false);
}

void AABaseWeapon::OnPickupOverlapBegin(UPrimitiveComponent* OverlappedComp, AActor* OtherActor, UPrimitiveComponent* OtherComp, int32 OtherBodyIndex, bool bFromSweep, const FHitResult& SweepResult)
{
	if (Role != ROLE_Authority)
	{
		return;
	}

	if (!bIsPickedUp && OtherActor->IsA(ADevTestTaskCharacter::StaticClass()))
	{
		Mul_OnPickup(OtherActor);
	}
}

void AABaseWeapon::Mul_OnPickup_Implementation(AActor* OtherActor)
{
	ADevTestTaskCharacter* character = Cast<ADevTestTaskCharacter>(OtherActor);
	playerOwner = character;
	character->PickupWeapon(this);

	SetOwner(character->GetController());

	bIsPickedUp = true;	
}

void AABaseWeapon::GetLifetimeReplicatedProps(TArray< FLifetimeProperty > & OutLifetimeProps) const
{
	Super::GetLifetimeReplicatedProps(OutLifetimeProps);

	DOREPLIFETIME(AABaseWeapon, applicableModules);
	DOREPLIFETIME(AABaseWeapon, activeModule);
	DOREPLIFETIME(AABaseWeapon, installedModules);

	DOREPLIFETIME(AABaseWeapon, damage);
	DOREPLIFETIME(AABaseWeapon, rpm);
	DOREPLIFETIME(AABaseWeapon, ammoCountTotal);
	DOREPLIFETIME(AABaseWeapon, ammoCountInMag);
	DOREPLIFETIME(AABaseWeapon, ammoCountInMagMax);
	DOREPLIFETIME(AABaseWeapon, reloadDuration);
	DOREPLIFETIME(AABaseWeapon, accuracy);

	DOREPLIFETIME(AABaseWeapon, bIsHidden);
}