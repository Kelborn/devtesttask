// Fill out your copyright notice in the Description page of Project Settings.

#include "ABaseWeaponModule.h"

#include "Components/SphereComponent.h"
#include "Components/StaticMeshComponent.h"
#include "DevTestTaskCharacter.h"
#include "Inventory.h"
#include "ABaseWeapon.h"
#include "Components/SkeletalMeshComponent.h"

// Sets default values
AABaseWeaponModule::AABaseWeaponModule()
{
 	// Set this actor to call Tick() every frame.  You can turn this off to improve performance if you don't need it.
	PrimaryActorTick.bCanEverTick = true;

	//setup static mesh
	staticMesh = CreateDefaultSubobject<UStaticMeshComponent>(FName("StaticMesh"));	
	staticMesh->SetCollisionResponseToAllChannels(ECollisionResponse::ECR_Ignore);
	RootComponent = staticMesh;

	//setup pickup collision
	pickupCollision = CreateDefaultSubobject<USphereComponent>(FName("PickupCollision"));
	pickupCollision->SetupAttachment(staticMesh);
	pickupCollision->SetSphereRadius(20.f);
	pickupCollision->SetCollisionResponseToAllChannels(ECollisionResponse::ECR_Ignore);
	pickupCollision->SetCollisionResponseToChannel(ECC_Pawn, ECollisionResponse::ECR_Overlap);
	pickupCollision->OnComponentBeginOverlap.AddDynamic(this, &AABaseWeaponModule::OnPickupOverlapBegin);

	//setup replication
	bReplicates = true;
	bReplicateMovement = true;
}

// Called when the game starts or when spawned
void AABaseWeaponModule::BeginPlay()
{
	Super::BeginPlay();
	
}

// Called every frame
void AABaseWeaponModule::Tick(float DeltaTime)
{
	Super::Tick(DeltaTime);

}

void AABaseWeaponModule::OnPickupOverlapBegin(UPrimitiveComponent* OverlappedComp, AActor* OtherActor, UPrimitiveComponent* OtherComp, int32 OtherBodyIndex, bool bFromSweep, const FHitResult& SweepResult)
{
	if (Role != ROLE_Authority)
	{
		return;
	}

	if (OtherActor->IsA(ADevTestTaskCharacter::StaticClass()))
	{
		AABaseWeapon* baseWeapon = GetApplicableWeapon(Cast<ADevTestTaskCharacter>(OtherActor));
		if (baseWeapon)
		{
			OnPickup(baseWeapon);
		}
	}
}

AABaseWeapon* AABaseWeaponModule::GetApplicableWeapon(ADevTestTaskCharacter* character)
{
	return character->inventory->FindWeapon(applicableWeapon);
}

void AABaseWeaponModule::OnPickup(AABaseWeapon* weapon)
{
	if (bIsInstalled)
	{
		return;
	}

	bIsInstalled = true;
	SetOwner(weapon->GetOwner());

	if (weapon->bIsHidden)
	{
		SetActorHiddenInGame(true);
	}

	AttachToComponent(weapon->skeletalMesh, FAttachmentTransformRules(EAttachmentRule::SnapToTarget, true), attachSocketTag);
	weapon->installedModules.Add(this);
}