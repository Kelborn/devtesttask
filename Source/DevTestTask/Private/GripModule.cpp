// Fill out your copyright notice in the Description page of Project Settings.

#include "GripModule.h"
#include "Weapons/ABaseWeapon.h"

void AGripModule::PutOnWeapon(AABaseWeapon* weapon)
{
	weapon->accuracy *= accuracyMul;
}

void AGripModule::RemoveFromWeapon(AABaseWeapon* weapon)
{
	weapon->accuracy /= accuracyMul;
}