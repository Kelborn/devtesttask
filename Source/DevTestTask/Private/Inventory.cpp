// Fill out your copyright notice in the Description page of Project Settings.

#include "Inventory.h"
#include "Weapons/ABaseWeapon.h"
#include "DevTestTaskCharacter.h"

// Sets default values for this component's properties
UInventory::UInventory()
{
	// Set this component to be initialized when the game starts, and to be ticked every frame.  You can turn these features
	// off to improve performance if you don't need them.
	PrimaryComponentTick.bCanEverTick = true;

	// ...
}


// Called when the game starts
void UInventory::BeginPlay()
{
	Super::BeginPlay();
	character = Cast<ADevTestTaskCharacter>(GetOwner());
	// ...
	
}


// Called every frame
void UInventory::TickComponent(float DeltaTime, ELevelTick TickType, FActorComponentTickFunction* ThisTickFunction)
{
	Super::TickComponent(DeltaTime, TickType, ThisTickFunction);

	// ...
}

void UInventory::Add(AABaseWeapon* weapon)
{
	weapons.Add(weapon);
	
	if (weapons.Num() == 1)
	{
		character->EquipWeapon(weapon);
	}
	else
	{
		weapon->Server_Hide();
	}
}

AABaseWeapon* UInventory::FindWeapon(UClass* weaponClass)
{
	for (AABaseWeapon* weapon : weapons)
	{
		if (weapon->GetClass() == weaponClass)
		{
			return weapon;
		}
	}

	return nullptr;
}

void UInventory::NextWeapon()
{
	int currentIndex = weapons.Find(currentWeapon);
	if (currentIndex == weapons.Num() - 1)
	{
		currentIndex = 0;
	}
	else
	{
		currentIndex++;
	}

	character->EquipWeapon(weapons[currentIndex]);
}

void UInventory::PrevWeapon()
{
	int currentIndex = weapons.Find(currentWeapon);
	if (currentIndex == 0)
	{
		currentIndex = weapons.Num() - 1;
	}
	else
	{
		currentIndex--;
	}

	character->EquipWeapon(weapons[currentIndex]);
}
