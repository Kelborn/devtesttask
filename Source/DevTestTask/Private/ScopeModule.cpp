// Fill out your copyright notice in the Description page of Project Settings.

#include "ScopeModule.h"

#include "Kismet/GameplayStatics.h"
#include "Engine/World.h"
#include "Weapons/ABaseWeapon.h"
#include "DevTestTaskCharacter.h"

#include "Engine.h"

void AScopeModule::Use(AABaseWeapon* weapon)
{
	if (!cameraManager)
	{
		ACharacter* character = weapon->playerOwner;
		if (!character->IsLocallyControlled())
		{
			return;
		}
		cameraManager = character->GetController<APlayerController>()->PlayerCameraManager;
	}

	if (bIsInUse)
	{
		cameraManager->SetFOV(oldFOV);
		weapon->accuracy /= accuracyMul;
	}
	else
	{
		oldFOV = cameraManager->GetFOVAngle();
		cameraManager->SetFOV(targetFOV);
		weapon->accuracy *= accuracyMul;
	}

	bIsInUse = !bIsInUse;
}

void AScopeModule::OnPickup(AABaseWeapon* weapon)
{
	Super::OnPickup(weapon);

	Client_BindWeaponChanged(weapon);
}

void AScopeModule::Client_BindWeaponChanged_Implementation(AABaseWeapon* weapon)
{
	if (weapon->playerOwner->IsLocallyControlled())
	{
		weapon->playerOwner->WeaponChangedDelegate.AddDynamic(this, &AScopeModule::OnWeaponChanged);
	}

}

void AScopeModule::OnWeaponChanged(AABaseWeapon* weapon)
{
	if (bIsInUse)
	{
		Use(weapon);
	}
}