// Fill out your copyright notice in the Description page of Project Settings.

#include "ActiveWeaponModule.h"
#include "ABaseWeapon.h"

void AActiveWeaponModule::TryUse(AABaseWeapon* weapon)
{
	if (CanUse())
	{
		Use(weapon);
	}
}

void AActiveWeaponModule::Use(AABaseWeapon* weapon)
{

}

bool AActiveWeaponModule::CanUse()
{
	return true;
}

void AActiveWeaponModule::OnPickup(AABaseWeapon* weapon)
{
	Super::OnPickup(weapon);
	weapon->activeModule = this;
}