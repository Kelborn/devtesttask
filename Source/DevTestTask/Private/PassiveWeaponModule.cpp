// Fill out your copyright notice in the Description page of Project Settings.

#include "PassiveWeaponModule.h"
#include "Weapons/ABaseWeapon.h"

void APassiveWeaponModule::PutOnWeapon(AABaseWeapon* weapon)
{

}

void APassiveWeaponModule::RemoveFromWeapon(AABaseWeapon* weapon)
{

}

void APassiveWeaponModule::OnPickup(AABaseWeapon* weapon)
{
	Super::OnPickup(weapon);
	PutOnWeapon(weapon);
}