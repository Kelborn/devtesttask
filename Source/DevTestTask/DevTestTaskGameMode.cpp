// Copyright 1998-2018 Epic Games, Inc. All Rights Reserved.

#include "DevTestTaskGameMode.h"
#include "DevTestTaskHUD.h"
#include "DevTestTaskCharacter.h"
#include "UObject/ConstructorHelpers.h"

ADevTestTaskGameMode::ADevTestTaskGameMode()
	: Super()
{
	// set default pawn class to our Blueprinted character
	static ConstructorHelpers::FClassFinder<APawn> PlayerPawnClassFinder(TEXT("/Game/FirstPersonCPP/Blueprints/FirstPersonCharacter"));
	DefaultPawnClass = PlayerPawnClassFinder.Class;

	// use our custom HUD class
	HUDClass = ADevTestTaskHUD::StaticClass();
}
